package main

import (
	"encoding/json"
	config2 "feedpipe/pkg/config"
	"feedpipe/pkg/feedpipe"
	"flag"
	"github.com/BurntSushi/toml"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"path"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	configPathPtr := flag.String("config-path", "", "path to config file. may be json or toml file.")
	flag.Parse()
	configPath := *configPathPtr
	if configPath == "" {
		logrus.Fatal("flag config-path is required.")
	}
	config := config2.Config{}
	switch path.Ext(configPath) {
	case ".json":
		data, err := ioutil.ReadFile(configPath)
		if err != nil {
			logrus.Fatalf("while reading file: %s", err)
		}
		err = json.Unmarshal(data, &config)
		if err != nil {
			logrus.Fatalf("while unmsrshalling JSON file: %s", err)
		}
	case ".toml":
		data, err := ioutil.ReadFile(configPath)
		if err != nil {
			logrus.Fatalf("while reading file: %s", err)
		}
		err = toml.Unmarshal(data, &config)
		if err != nil {
			logrus.Fatalf("while unmsrshalling TOML file: %s", err)
		}
	default:
		logrus.Fatalf("config file extension %s not supported", path.Ext(configPath))
	}
	for {
		for i, pipe := range config.Pipes {
			dests := make([]feedpipe.Dest, len(pipe.Dests))
			for i, dest := range pipe.Dests {
				dests[i] = dest.Email
			}
			err := feedpipe.Pipe{
				FeedLink: pipe.Feed,
				Dests:    dests,
			}.Pipe()
			if err != nil {
				logrus.Warnf("failed to send digest from %s: %s", pipe.Feed, err)
			}
			logrus.Debugf("Sent pipe %v.", i)
		}
		logrus.Infof("Sent using %v pipe(s).", len(config.Pipes))
	}
}
