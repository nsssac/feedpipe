package feedpipe

import (
	"crypto/tls"
	"fmt"
	"github.com/mmcdole/gofeed"
	"gopkg.in/mail.v2"
)

type Dest interface {
	Send(string, string) error
}

type EmailDest struct {
	FromAddr string `json:"fromAddr" toml:"fromAddr"`
	FromPass string `json:"fromPass" toml:"fromPass"`

	SMTPHost string `json:"smtpHost" toml:"smtpHost"`
	SMTPPort int    `json:"smtpPort" toml:"smtpPort"`

	ToAddr string `json:"toAddr" toml:"toAddr"`
}

func (e EmailDest) Send(title, body string) error {
	msg := mail.NewMessage()
	msg.SetHeader("From", e.FromAddr)
	msg.SetHeader("To", e.ToAddr)
	msg.SetHeader("Subject", title)
	msg.SetBody("text/plain", body)
	dialer := mail.NewDialer(e.SMTPHost, e.SMTPPort, e.FromAddr, e.FromPass)
	dialer.TLSConfig = &tls.Config{
		MinVersion: tls.VersionTLS13,
		ServerName: e.SMTPHost,
	}
	return dialer.DialAndSend(msg)
}

type Pipe struct {
	FeedLink string
	Dests    []Dest
}

func (p Pipe) Pipe() error {
	parser := gofeed.NewParser()
	feed, err := parser.ParseURL(p.FeedLink)
	if err != nil {
		return err
	}
	body := "<ol>" // TODO: add support for text/template or html/template
	for _, item := range feed.Items {
		body += fmt.Sprintf(`  <li><a href="%s">%s</a></li>`, item.Link, item.Title)
	}
	body += "</ol>"
	for _, dest := range p.Dests {
		err = dest.Send(fmt.Sprintf("Weekly Digest for %s", feed.Title), body)
		if err != nil {
			return err
		}
	}
	return nil
}
