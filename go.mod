module feedpipe

go 1.15

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/mmcdole/gofeed v1.1.0
	github.com/sirupsen/logrus v1.7.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1
)
